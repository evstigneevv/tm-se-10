package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.exception.LoginException;

public class UserLogInCommand extends AbstractCommand {

    @Override
    public String command() {
        return "LI";
    }

    @Override
    public String description() {
        return "Changes current user";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getCurrentUser() != null) {
            throw new LoginException("You need to log out first!");
        }
        System.out.println("enter user login");
        @NotNull final String login = bootstrap.getScanner().nextLine();
        System.out.println("enter user password");
        @NotNull final String password = bootstrap.getScanner().nextLine();
        if (bootstrap.getUserService().checkPassword(login, password)) {
            bootstrap.setCurrentUser(bootstrap.getUserService().findByLogin(login));
            System.out.println("You successfully logged in!");
        } else {
            System.out.println("Wrong password!");
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
