package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Task;

public class TaskSearchByString extends AbstractCommand {

    @Override
    public String command() {
        return "STS";
    }

    @Override
    public String description() {
        return "Search tasks by string";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter a string for search: ");
        String searchingString = bootstrap.getScanner().nextLine();
        for (Task t : bootstrap.getTaskService().searchByString(searchingString)) {
            System.out.println(t.getName() + t.getDescription() + bootstrap.getDateParser().getDateString(t.getDateFinish())
                    + t.getStatus() + bootstrap.getDateParser().getDateString(t.getDateFinish()) + t.getDateOfCreation()
                    + t.getUserId() + t.getId());
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
