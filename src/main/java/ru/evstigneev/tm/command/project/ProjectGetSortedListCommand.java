package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.IProjectService;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;

public class ProjectGetSortedListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "PGSL";
    }

    @Override
    public String description() {
        return "Show sorted by you choice list of projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("date start \ndate finish \ndate creation \nstatus ");
        System.out.println("Enter by which value sort: ");
        String comparatorName = bootstrap.getScanner().nextLine();
        @NotNull final IProjectService projectService = bootstrap.getProjectService();
        for (Project p : projectService.sort(comparatorName)) {
            System.out.println(p.getUserId() + " userId | " + p.getId() + " id | " + p.getName() + " name | "
                    + bootstrap.getDateParser().getDateString(p.getDateStart()) +
                    " start" +
                    " date | " + bootstrap.getDateParser().getDateString(p.getDateFinish()) + " finish date | "
                    + p.getStatus().displayName() + " status | "
            );
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
