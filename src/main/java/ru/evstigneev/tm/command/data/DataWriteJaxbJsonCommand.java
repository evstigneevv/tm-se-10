package ru.evstigneev.tm.command.data;

import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DataWriteJaxbJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JAXB JSON";
    }

    @Override
    public String description() {
        return "Save current data to .json file";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext jaxbContextUsers = JAXBContext.newInstance(Users.class);
        @NotNull final Marshaller marshallerUser = jaxbContextUsers.createMarshaller();
        marshallerUser.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerUser.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        marshallerUser.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @NotNull final File fileUsers = new File("src/main/file/users.json");
        @NotNull final Users users = new Users();
        users.setUsers(bootstrap.getUserService().findAll());
        marshallerUser.marshal(users, fileUsers);
        @NotNull final JAXBContext jaxbContextProjects = JAXBContext.newInstance(Projects.class);
        @NotNull final Marshaller marshallerProjects = jaxbContextProjects.createMarshaller();
        marshallerProjects.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerProjects.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        marshallerProjects.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @NotNull final File fileProjects = new File("src/main/file/projects.json");
        @NotNull final Projects projects = new Projects();
        projects.setProjects(bootstrap.getProjectService().findAll());
        marshallerProjects.marshal(projects, fileProjects);
        @NotNull final JAXBContext jaxbContextTasks = JAXBContext.newInstance(Tasks.class);
        @NotNull final Marshaller marshallerTasks = jaxbContextTasks.createMarshaller();
        marshallerTasks.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerTasks.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        marshallerTasks.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @NotNull final File fileTasks = new File("src/main/file/tasks.json");
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(bootstrap.getTaskService().findAll());
        marshallerTasks.marshal(tasks, fileTasks);
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
