package ru.evstigneev.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    READY("READY"),
    IN_PROCESS("IN_PROCESS"),
    PLANING("PLANING");

    private String status;

    Status(@NotNull final String status) {
        this.status = status;
    }

    public String displayName() {
        return status;
    }
}
