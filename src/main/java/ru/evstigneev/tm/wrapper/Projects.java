package ru.evstigneev.tm.wrapper;

import lombok.Getter;
import lombok.Setter;
import ru.evstigneev.tm.entity.Project;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement(name = "projects")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Projects {

    private Collection<Project> projects;

}
