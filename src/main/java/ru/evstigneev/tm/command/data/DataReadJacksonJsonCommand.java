package ru.evstigneev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import java.io.File;

public class DataReadJacksonJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JACKSON JSON";
    }

    @Override
    public String description() {
        return "Read data .json file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final Users users = userMapper.readValue(new File("src/main/file/usersJackson.json"), Users.class);
        for (User u : users.getUsers()) {
            bootstrap.getUserService().persist(u.getId(), u);
        }
        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final Projects projects = projectMapper.readValue(new File("src/main/file/projectsJackson.json"), Projects.class);
        for (Project p : projects.getProjects()) {
            bootstrap.getProjectService().persist(p.getUserId(), p);
        }
        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final Tasks tasks = taskMapper.readValue(new File("src/main/file/tasksJackson.json"), Tasks.class);
        for (Task t : tasks.getTasks()) {
            bootstrap.getTaskService().persist(t.getUserId(), t);
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
