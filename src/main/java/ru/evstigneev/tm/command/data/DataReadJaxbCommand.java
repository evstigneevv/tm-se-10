package ru.evstigneev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataReadJaxbCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JAXB";
    }

    @Override
    public String description() {
        return "Read data from xml file";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext jaxbContextUser = JAXBContext.newInstance(Users.class);
        @NotNull final Unmarshaller unmarshallerUser = jaxbContextUser.createUnmarshaller();
        @NotNull final Users users = (Users) unmarshallerUser.unmarshal(new File("src/main/file/users.xml"));
        for (User u : users.getUsers()) {
            bootstrap.getUserService().persist(u.getId(), u);
        }
        @NotNull final JAXBContext jaxbContextProject = JAXBContext.newInstance(Projects.class);
        @NotNull final Unmarshaller unmarshallerProject = jaxbContextProject.createUnmarshaller();
        @NotNull final Projects projects = (Projects) unmarshallerProject.unmarshal(new File("src/main/file/projects" +
                ".xml"));
        for (Project p : projects.getProjects()) {
            bootstrap.getProjectService().persist(p.getUserId(), p);
        }
        @NotNull final JAXBContext jaxbContextTask = JAXBContext.newInstance(Tasks.class);
        @NotNull final Unmarshaller unmarshallerTask = jaxbContextTask.createUnmarshaller();
        @NotNull final Tasks tasks = (Tasks) unmarshallerTask.unmarshal(new File("src/main/file/tasks.xml"));
        for (Task t : tasks.getTasks()) {
            bootstrap.getTaskService().persist(t.getUserId(), t);
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
