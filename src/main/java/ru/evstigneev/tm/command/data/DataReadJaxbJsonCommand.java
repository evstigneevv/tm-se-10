package ru.evstigneev.tm.command.data;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataReadJaxbJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JAXB JSON";
    }

    @Override
    public String description() {
        return "Read data from .json file";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext jaxbContextUsers = JAXBContext.newInstance(Users.class);
        @NotNull final Unmarshaller unmarshallerUsers = jaxbContextUsers.createUnmarshaller();
        unmarshallerUsers.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshallerUsers.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final Users users = (Users) unmarshallerUsers.unmarshal(new File("src/main/file/users.json"));
        if (users != null) {
            for (User u : users.getUsers()) {
                bootstrap.getUserService().persist(u.getId(), u);
            }
        }
        @NotNull final JAXBContext jaxbContextProjects = JAXBContext.newInstance(Projects.class);
        @NotNull final Unmarshaller unmarshallerProjects = jaxbContextProjects.createUnmarshaller();
        unmarshallerProjects.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshallerProjects.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final Projects projects = (Projects) unmarshallerProjects.unmarshal(new File("src/main/file" +
                "/projects.json"));
        if (projects != null) {
            for (Project p : projects.getProjects()) {
                bootstrap.getProjectService().persist(p.getUserId(), p);
            }
        }
        @NotNull final JAXBContext jaxbContextTasks = JAXBContext.newInstance(Tasks.class);
        @NotNull final Unmarshaller unmarshallerTasks = jaxbContextTasks.createUnmarshaller();
        unmarshallerTasks.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshallerTasks.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final Tasks tasks = (Tasks) unmarshallerTasks.unmarshal(new File("src/main/file/tasks.json"));
        if (tasks != null) {
            for (Task t : tasks.getTasks()) {
                bootstrap.getTaskService().persist(t.getUserId(), t);
            }
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
