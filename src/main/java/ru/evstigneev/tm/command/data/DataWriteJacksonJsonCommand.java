package ru.evstigneev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import java.io.File;

public class DataWriteJacksonJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JACKSON JSON";
    }

    @Override
    public String description() {
        return "Write data to .json file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final Users users = new Users();
        users.setUsers(bootstrap.getUserService().findAll());
        userMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/usersJackson.json"), users);
        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final Projects projects = new Projects();
        projects.setProjects(bootstrap.getProjectService().findAll());
        projectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/projectsJackson.json"),
                projects);
        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(bootstrap.getTaskService().findAll());
        taskMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/tasksJackson.json"),
                tasks);
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}