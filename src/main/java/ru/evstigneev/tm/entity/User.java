package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.enumerated.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class User implements Serializable {

    @NotNull
    private String login;
    @NotNull
    private String id;
    @NotNull
    private String passwordHash;
    @NotNull
    private RoleType role;

}
