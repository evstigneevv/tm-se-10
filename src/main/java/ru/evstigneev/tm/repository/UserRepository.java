package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.IUserRepository;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.util.PasswordParser;

import java.util.Collection;
import java.util.UUID;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(@NotNull final String login, @NotNull final String password) throws EmptyStringException {
        if (login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setId(UUID.randomUUID().toString());
        user.setRole(RoleType.ADMIN);
        entities.put(user.getId(), user);
        return user;
    }

    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws EmptyStringException {
        if (login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setId(UUID.randomUUID().toString());
        user.setRole(role);
        entities.put(user.getId(), user);
        return user;
    }

    @Override
    public boolean updatePassword(@NotNull final String login, @NotNull final String newPassword) throws EmptyStringException {
        if (login.isEmpty() || newPassword.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable final Collection<User> userCollection = entities.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                entities.get(u.getId()).setPasswordHash(PasswordParser.getPasswordHash(newPassword));
                return true;
            }
        }
        return false;
    }

    @Override
    public User findByLogin(@NotNull final String login) throws EmptyStringException {
        if (login.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable final Collection<User> userCollection = entities.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                return u;
            }
        }
        return null;
    }

    @Override
    public Collection<User> findAll() {
        return entities.values();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String userName) throws EmptyStringException {
        if (userId.isEmpty() || userName.isEmpty()) {
            throw new EmptyStringException();
        }
        entities.get(userId).setLogin(userName);
    }

    @Override
    public boolean remove(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return entities.remove(userId) != null;
    }

    @Override
    public User merge(@NotNull final String userId, @NotNull final User user) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(userId).getRole().equals(RoleType.ADMIN)) {
            return entities.put(userId, user);
        }
        return persist(userId, user);
    }

    @Override
    public User persist(@NotNull final String userId, @NotNull final User user) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (user.getId().equals(userId)) {
            return entities.put(userId, user);
        }
        return null;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}