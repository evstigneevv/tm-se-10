package ru.evstigneev.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.util.DateParser;

import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    User getCurrentUser();

    void setCurrentUser(@Nullable final User currentUser);

    Scanner getScanner();

    Collection<AbstractCommand> getCommands();

    DateParser getDateParser();

    void init() throws EmptyStringException;

}
