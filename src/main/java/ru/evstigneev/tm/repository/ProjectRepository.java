package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.IProjectRepository;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(@NotNull final String userId, @NotNull final String projectName) throws EmptyStringException {
        if (userId.isEmpty() || projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        Project project = new Project();
        project.setUserId(userId);
        project.setName(projectName);
        project.setId(UUID.randomUUID().toString());
        project.setDateStart(new Date());
        entities.put(project.getId(), project);
        return project;
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(projectId).getUserId().equals(userId)) {
            return entities.remove(projectId) != null;
        } else return false;
    }

    @Override
    public Project update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String projectName) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty() || projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(projectId).getUserId().equals(userId)) {
            entities.get(projectId).setName(projectName);
        }
        return entities.get(projectId);
    }

    @Override
    public Collection<Project> findAll() {
        return entities.values();
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable Map<String, Project> userProjects = new LinkedHashMap<>();
        for (Map.Entry<String, Project> entry : entities.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                userProjects.put(entry.getKey(), entry.getValue());
        }
        return userProjects.values();
    }

    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(projectId).getUserId().equals(userId)) {
            return entities.get(projectId);
        }
        return null;
    }

    @Override
    public Project merge(@NotNull final String userId, @NotNull final Project project) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(project.getId()).getUserId().equals(userId)) {
            return entities.put(project.getId(), project);
        }
        return null;
    }

    @Override
    public Project persist(@NotNull final String userId, @NotNull final Project project) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (project.getUserId().equals(userId)) {
            return entities.put(project.getId(), project);
        }
        return null;
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        boolean isRemoved = false;
        for (Project p : findAllByUserId(userId)) {
            if (p.getUserId().equals(userId)) {
                entities.remove(p.getId());
                isRemoved = true;
            }
        }
        return isRemoved;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}
