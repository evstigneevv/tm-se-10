package ru.evstigneev.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.ServiceLocator;
import ru.evstigneev.tm.enumerated.RoleType;

public abstract class AbstractCommand {

    protected ServiceLocator bootstrap;

    public void setBootstrap(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    public abstract boolean requiredAuth() throws Exception;

    public boolean havePermission(@NotNull final String first, @NotNull final String second) throws Exception {
        return true;
    }

    public boolean havePermission(@NotNull final String first, @NotNull final String second,
                                  @NotNull final String third) throws Exception {
        return true;
    }

}