package ru.evstigneev.tm.command.user;

import ru.evstigneev.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "LO";
    }

    @Override
    public String description() {
        return "Log out current user";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.setCurrentUser(null);
        System.out.println("You log out successfully!");
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
