package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.ITaskService;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Task;

public class TaskGetSortedListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "TGSL";
    }

    @Override
    public String description() {
        return "Show sorted list of tasks: ";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("date start \ndate finish \ndate creation \nstatus ");
        System.out.println("Enter by which value sort: ");
        String comparatorName = bootstrap.getScanner().nextLine();
        @NotNull final ITaskService taskService = bootstrap.getTaskService();
        for (Task t : taskService.sort(comparatorName)) {
            System.out.println(t.getUserId() + " userId | " + t.getId() + " id | " + t.getName() + " name | "
                    + bootstrap.getDateParser().getDateString(t.getDateStart()) +
                    " start" +
                    " date | " + bootstrap.getDateParser().getDateString(t.getDateFinish()) + " finish date | "
                    + t.getStatus().displayName() + " status | "
            );
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
