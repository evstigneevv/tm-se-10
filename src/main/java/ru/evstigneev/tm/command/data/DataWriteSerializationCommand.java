package ru.evstigneev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class DataWriteSerializationCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE";
    }

    @Override
    public String description() {
        return "Saving current data to file";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("src/main/file/data.ser"));
        @Nullable final List<User> userList = new ArrayList<>(bootstrap.getUserService().findAll());
        @Nullable final List<Project> projectList = new ArrayList<>(bootstrap.getProjectService().findAll());
        @Nullable final List<Task> taskList = new ArrayList<>(bootstrap.getTaskService().findAll());
        objectOutputStream.writeObject(userList);
        objectOutputStream.writeObject(projectList);
        objectOutputStream.writeObject(taskList);
        objectOutputStream.close();
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
