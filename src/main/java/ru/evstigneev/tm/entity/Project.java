package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project implements Comparable<Project>, Serializable {

    @NotNull
    final private Date dateOfCreation = new Date();
    @NotNull
    private String name;
    @NotNull
    private String id;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private String userId;
    @NotNull
    private Status status = Status.PLANING;

    @Override
    public int compareTo(@NotNull Project o) {
        return this.getStatus().compareTo(o.getStatus());
    }

}
