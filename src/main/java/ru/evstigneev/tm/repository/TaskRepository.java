package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskName) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        {
            Task task = new Task();
            task.setUserId(userId);
            task.setProjectId(projectId);
            task.setName(taskName);
            task.setId(UUID.randomUUID().toString());
            return entities.put(task.getId(), task);
        }
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException {
        if (userId.isEmpty() || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(taskId).getUserId().equals(userId)) {
            return entities.remove(taskId) != null;
        }
        return false;
    }

    @Override
    public Task update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String taskName) throws EmptyStringException {
        if (userId.isEmpty() || taskId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(taskId).getUserId().equals(userId)) {
            entities.get(taskId).setName(taskName);
        }
        return entities.get(taskId);
    }

    @Override
    public Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable List<Task> taskListByProjectId = new ArrayList<>();
        @Nullable final Collection<Task> taskList = entities.values();
        for (Task t : taskList) {
            if (t.getProjectId().equals(projectId) && t.getUserId().equals(userId)) {
                taskListByProjectId.add(t);
            }
        }
        return taskListByProjectId;
    }

    @Override
    public Collection<Task> findAll() {
        return entities.values();
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable Map<String, Task> userTasks = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : entities.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                userTasks.put(entry.getKey(), entry.getValue());
        }
        return userTasks.values();
    }

    @Override
    public boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        boolean isDeleted = false;
        @Nullable final Collection<Task> projectTasks = entities.values();
        for (Task t : projectTasks) {
            if (t.getProjectId().equals(projectId) && t.getUserId().equals(userId)) {
                entities.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty() || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (entities.get(taskId).getUserId().equals(userId) && entities.get(taskId).getProjectId().equals(projectId)) {
            return entities.get(taskId);
        }
        return null;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        boolean isDeleted = false;
        @Nullable final Collection<Task> projectTasks = entities.values();
        for (Task t : projectTasks) {
            if (t.getUserId().equals(userId)) {
                entities.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    @Override
    public Task merge(@NotNull final String userId, @NotNull final Task task) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (findOne(userId, task.getProjectId(), task.getId()) != null) {
            entities.put(task.getId(), task);
        }
        return persist(userId, task);
    }

    @Override
    public Task persist(@NotNull final String userId, @NotNull final Task task) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (task.getUserId().equals(userId)) {
            return entities.put(task.getId(), task);
        }
        return null;
    }

}