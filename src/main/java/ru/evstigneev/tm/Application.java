package ru.evstigneev.tm;

import org.apache.log4j.BasicConfigurator;
import ru.evstigneev.tm.api.ServiceLocator;
import ru.evstigneev.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        ServiceLocator bootstrap = new Bootstrap();
        bootstrap.init();
    }

}