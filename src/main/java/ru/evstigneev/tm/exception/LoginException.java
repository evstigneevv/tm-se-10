package ru.evstigneev.tm.exception;

public class LoginException extends Exception {

    public LoginException() {
        super("You need to login first!");
    }

    public LoginException(String message) {
        super(message);
    }

}
