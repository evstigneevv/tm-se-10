package ru.evstigneev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

public class DataReadSerializationCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ";
    }

    @Override
    public String description() {
        return "Read data from file";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("src/main/file/data.ser"));
        @Nullable final List<User> users = (List<User>) objectInputStream.readObject();
        @Nullable final List<Project> projects = (List<Project>) objectInputStream.readObject();
        @Nullable final List<Task> tasks = (List<Task>) objectInputStream.readObject();
        if (users != null) {
            for (User u : users) {
                bootstrap.getUserService().persist(u.getId(), u);
            }
        }
        if (projects != null) {
            for (Project p : projects) {
                @NotNull final String userId = p.getUserId();
                bootstrap.getProjectService().persist(userId, p);
            }
        }
        if (tasks != null) {
            for (Task t : tasks) {
                @NotNull final String userId = t.getUserId();
                bootstrap.getTaskService().persist(userId, t);
            }
        }
        objectInputStream.close();
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
