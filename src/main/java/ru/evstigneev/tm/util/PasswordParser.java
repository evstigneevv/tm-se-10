package ru.evstigneev.tm.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordParser {

    public static String getPasswordHash(String passwordInput) {
        byte[] array = new byte[0];
        try {
            array = MessageDigest.getInstance("MD5").digest(passwordInput.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        for (byte b : array) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100));
        }
        return sb.toString();
    }

}
