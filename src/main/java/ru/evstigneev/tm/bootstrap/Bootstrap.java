package ru.evstigneev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.evstigneev.tm.api.*;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.LoginException;
import ru.evstigneev.tm.exception.NoPermissionException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;
import ru.evstigneev.tm.service.UserService;
import ru.evstigneev.tm.util.DateParser;

import java.util.*;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(new ProjectRepository(), taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final IUserService userService = new UserService(new UserRepository());
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.evstigneev.tm").getSubTypesOf(AbstractCommand.class);
    @NotNull
    private final DateParser dateParser = new DateParser();
    @Nullable
    private User currentUser;

    public void init() throws EmptyStringException {
        for (Class clazz : classes) {
            registry(clazz);
        }
        start();
    }

    private void registry(@NotNull final Class clazz) {
        if (clazz.getSuperclass().equals(AbstractCommand.class)) {
            try {
                @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
                command.setBootstrap(this);
                registry(command);
            } catch (InstantiationException | IllegalAccessException | CommandCorruptException e) {
                e.printStackTrace();
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @NotNull final String commandText = command.command();
        @NotNull final String description = command.description();
        if (commandText.isEmpty() || description.isEmpty())
            throw new CommandCorruptException();
        commands.put(commandText, command);
    }

    private void start() {
        System.out.println("Welcome to Task Manager!");
        System.out.print("input command or \"EXIT\" to exit...");
        while (true) {
            @NotNull final String command = scanner.nextLine().toUpperCase();
            try {
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty()) {
            throw new EmptyStringException();
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandCorruptException();
        }
        if (abstractCommand.requiredAuth() && currentUser == null) {
            throw new LoginException();
        }
        if (abstractCommand.requiredAuth()) {
            @NotNull final Collection<RoleType> roleTypeCollection = Arrays.asList(abstractCommand.getSupportedRoles());
            if (roleTypeCollection.contains(currentUser.getRole())) {
                abstractCommand.execute();
            } else {
                throw new NoPermissionException();
            }
        } else {
            abstractCommand.execute();
        }
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }

    @NotNull
    public DateParser getDateParser() {
        return dateParser;
    }

}
