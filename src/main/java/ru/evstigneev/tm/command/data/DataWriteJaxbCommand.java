package ru.evstigneev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DataWriteJaxbCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JAXB";
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext jaxbContextUser = JAXBContext.newInstance(Users.class);
        @NotNull final Marshaller marshallerUser = jaxbContextUser.createMarshaller();
        marshallerUser.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File fileUsers = new File("src/main/file/users.xml");
        @NotNull final Users users = new Users();
        users.setUsers(bootstrap.getUserService().findAll());
        marshallerUser.marshal(users, fileUsers);
        @NotNull final JAXBContext jaxbContextProject = JAXBContext.newInstance(Projects.class);
        @NotNull final Marshaller marshallerProject = jaxbContextProject.createMarshaller();
        marshallerProject.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File fileProjects = new File("src/main/file/projects.xml");
        @NotNull final Projects projects = new Projects();
        projects.setProjects(bootstrap.getProjectService().findAll());
        marshallerProject.marshal(projects, fileProjects);
        @NotNull final JAXBContext jaxbContextTask = JAXBContext.newInstance(Tasks.class);
        @NotNull final Marshaller marshallerTask = jaxbContextTask.createMarshaller();
        marshallerTask.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File fileTasks = new File("src/main/file/tasks.xml");
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(bootstrap.getTaskService().findAll());
        marshallerTask.marshal(tasks, fileTasks);
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
