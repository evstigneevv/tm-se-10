package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UU";
    }

    @Override
    public String description() {
        return "Update current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter new user name: ");
        @NotNull final String input = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().update(bootstrap.getCurrentUser().getId(), input);
        System.out.println("User name has changed!");
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}

