package ru.evstigneev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import java.io.File;

public class DataWriteJacksonXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JACKSON XML";
    }

    @Override
    public String description() {
        return "Write data to .xml file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper userMapper = new XmlMapper();
        @NotNull final Users users = new Users();
        users.setUsers(bootstrap.getUserService().findAll());
        userMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/usersJackson.xml"), users);
        @NotNull final ObjectMapper projectMapper = new XmlMapper();
        @NotNull final Projects projects = new Projects();
        projects.setProjects(bootstrap.getProjectService().findAll());
        projectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/projectsJackson.xml"),
                projects);
        @NotNull final ObjectMapper taskMapper = new XmlMapper();
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(bootstrap.getTaskService().findAll());
        taskMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/tasksJackson.xml"),
                tasks);
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
